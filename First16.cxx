#include <iostream>
#include "TFile.h"
#include "TH1D.h"
#include "TH1F.h"
#include "TMath.h"
#include "TAxis.h"
#include "TPad.h"
#include "TROOT.h"
#include "TPaveLabel.h"

using namespace std;

TObject* comparettc(const char* Str1, TTree* ttree);
TObject* comparettlight(const char* Str2, TTree* ttree);
TObject* comparettb(const char* Str3, TTree* ttree);
int comparePlotter(TObject* ttcOb, TObject* ttlightOb, TObject* ttbOb, string varName);
 
void First16() {
  //TH1::AddDirectory(kFALSE);
  //open the file
  TFile *f1 = new TFile("/home/ppe/m/mawaji/glasgow-ana-ma/glasgow-ana/GlasgowAnaCore/share/output/outputs_1516/mergedOutput/ttHBoosted2016/ttallpp8.root");
  TTree *ttree = (TTree*)f1->Get("nominal_Loose"); //define the histogram pointer

  string ttVaraible[8] = {"rcjet_m","rcjet_pt","rcjet_phi", "rcjet_eta", "lep_m","lep_pt","lep_phi","lep_eta"};
  stringstream strStream1;
  stringstream strStream2;
  stringstream strStream3;

  TString Str1, Str2, Str3;
  TObject *ttc, *ttb, *ttlight;

  for(int i = 0; i < 8; i++) {
    strStream1.clear();
    strStream1.str("");
    strStream1  << ttVaraible[i]; 
    strStream1 << " >> ttc";
 
    strStream2.clear();
    strStream2.str("");
    strStream2  << ttVaraible[i]; 
    strStream2 << " >> ttlight";
 
    strStream3.clear();
    strStream3.str("");
    strStream3  << ttVaraible[i]; 
    strStream3 << " >> ttb";
 
        
    Str1 = strStream1.str();
    ttc = comparettc(Str1, ttree);

    Str2 = strStream2.str();
    ttlight = comparettlight(Str2, ttree);
    
    Str3 = strStream3.str();
    ttb = comparettb(Str3, ttree);
    /*	
    if(i==0){
    TCanvas *test1 = new TCanvas("test1","ncanvas1",600,600);
    test1->cd();
    ttc->Draw();
    ttlight->Draw("SAME");
    ttb->Draw("SAME");
    //test1->SaveAs("test1.pdf","pdf");
    }
    
    if (i==1){
    TCanvas *test2 = new TCanvas("test2","ncanvas2",600,600);
    test2->cd();
    ttc->Draw();
    ttlight->Draw("SAME");
    ttb->Draw("SAME");
    //test2->SaveAs("test2.pdf","pdf");
    }
    
    if (i==2){
    TCanvas *test3 = new TCanvas("test3","ncanvas3",600,600);
    test3->cd();
    ttc->Draw();
    ttlight->Draw("SAME");
    ttb->Draw("SAME");
    //test3->SaveAs("test3.pdf","pdf");
    }
    */


   

    int multiPlot2 = comparePlotter(ttc, ttlight,ttb, ttVaraible[i] );
}
}

int comparePlotter(TObject* ttcOb, TObject* ttlightOb, TObject* ttbOb, string varName) {
  
  stringstream varNameStream;
  varNameStream << varName;
  TString varT = varNameStream.str();

  stringstream streamTitle;
  streamTitle << varName;
  streamTitle << " (ttbar sample)";
  TString myTTitle = streamTitle.str();

  bareVar=varName.substr(varName.find_first_of("_")+1);
  stringstream bareVarStream;
  bareVarStream << bareVar;
  TString myTBareVar = bareVarStream.str();

  stringstream saveAsStream;
  saveAsStream << varName;
  saveAsStream << ".pdf";
  TString saveAsT = saveAsStream.str();


  TH1D *ttc = (TH1D*) ttcOb;
  TH1D *ttlight = (TH1D*) ttlightOb;
  TH1D *ttb = (TH1D*) ttbOb;


  //  c1->Divide(1,2);
  
  
  //   c1->SetFillColor(18);
  
  ttc->SetMarkerColor(kGreen+0);
  ttc->SetMarkerStyle(6);
  ttc->SetLineStyle(1);
  ttc->SetLineColor(3);
  ttc->SetFillColor(kGreen-10);
  ttlight->SetMarkerColor(kRed+0);
  ttlight->SetMarkerStyle(1);
  ttlight->SetLineStyle(1);
  ttlight->SetLineColor(2);
  ttlight->SetFillColor(kRed-10);
  ttb->SetMarkerColor(kBlue+0);
  ttb->SetMarkerStyle(7);
  ttb->SetLineStyle(1);
  ttb->SetLineColor(4);
  ttb->SetFillColor(kBlue-10);
  ttc->SetFillStyle(3001);
  ttlight->SetFillStyle(3001);
  ttb->SetFillStyle(3001);
  ttc->GetXaxis()->SetTitle(myTBareVar);
  ttc->GetYaxis()->SetTitle("Rcjet-event");
  
    TCanvas *c1 = new TCanvas("c1","canvas1",600,600);
    c1->cd();
    gPad->SetFillColor(18);
    ttc->Draw();
    ttlight->Draw("SAME");
    ttb->Draw("SAME");
    
    
    //  c1->Update();
  
  auto *legend = new TLegend(0.7,0.75,0.98,0.935);
  legend->SetHeader(varT, "C");
  legend->AddEntry(ttc,"ttc","lep");
  legend->AddEntry(ttlight,"ttlight","lep");
  legend->AddEntry(ttb,"ttb","lep");
  legend->Draw();
  
  gStyle->SetOptTitle(0);
  TPaveLabel *title = new TPaveLabel(0.38,0.92,0.67,0.98,myTTitle,"bRNDC");
  title->Draw();
  
  //char saveAs = static_cast<char>(i);
  //char* saveAs2 = &saveAs;
 
  c1->SaveAs(saveAsT,"pdf");

  /*
  if (i==0)  c1->SaveAs("11.pdf","pdf");
  if (i==1)  c1->SaveAs("12.pdf","pdf");
  if (i==2)  c1->SaveAs("13.pdf","pdf");
  if (i==3)  c1->SaveAs("14.pdf","pdf");  
  if (i==4)  c1->SaveAs("15.pdf","pdf");
  if (i==5)  c1->SaveAs("16.pdf","pdf");  
  
  
  if (i==0)  c1->SaveAs(varT,"pdf");
  if (i==1)  c1->SaveAs(varT,"pdf");
  if (i==2)  c1->SaveAs(varT,"pdf");
  if (i==3)  c1->SaveAs(varT,"pdf");
  if (i==4)  c1->SaveAs(varT,"pdf");
  if (i==5)  c1->SaveAs(varT,"pdf");
  */
/*
    if(i == 0) {
      TCanvas *c1 = new TCanvas("c1","canvas1",600,600);
      c1->cd();
      ttc->Draw();
      ttlight->Draw("SAME");
      ttb->Draw("SAME");
      c1->SaveAs("11.pdf","pdf");
    }
  
  if(i == 1){
    TCanvas *c2 = new TCanvas("c2","ttc",600,600);
    c2->cd();
    ttc->Draw();
    ttlight->Draw("SAME");
    ttb->Draw("SAME");
    c2->SaveAs("12.pdf","pdf");
    }
  
  if(i == 2){
    TCanvas *c3 = new TCanvas("c3","ttc",600,600);
  
    c3->SaveAs("13.pdf","pdf");
  }
  if(i == 3){
    TCanvas *c4 = new TCanvas("c4","ttc",600,600);
    c4->SaveAs("14.pdf","pdf");
  }
  */

  return 0;
}


TObject* comparettc (const char* Str1, TTree* ttree) {

  gDirectory->Clear();
  //  TH1D *ttc = (TH1D*) gDirectory->Get("ttc");
  ttree->Draw(Str1,"weight_normalise && HF_SimpleClassification==-1","goff");
  //  ttc = (TH1D*) gDirectory->Get("ttc");
  //  ttc->SetDirectory(0);

  return gDirectory->Get("ttc");
}
TObject* comparettlight (const char* Str2, TTree* ttree) {
gDirectory->Clear();
  //TH1D *ttlight = (TH1D*) gDirectory->Get("ttlight");
  ttree->Draw(Str2,"weight_normalise && HF_SimpleClassification==0","goff");
  // ttlight = (TH1D*) gDirectory->Get("ttlight");
  // ttlight->SetDirectory(0);

  return gDirectory->Get("ttlight");
}

TObject* comparettb(const char* Str3, TTree* ttree) {
 
  gDirectory->Clear();
  // TH1D *ttb = (TH1D*) gDirectory->Get("ttb");
  ttree->Draw(Str3,"weight_normalise && HF_SimpleClassification==1","goff");
  // ttb = (TH1D*) gDirectory->Get("ttb");
  //ttb->SetDirectory(0);

  return gDirectory->Get("ttb");
}
